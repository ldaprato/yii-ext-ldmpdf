<?php
/**
 * 
 * @author Louis A. DaPrato <l.daprato@gmail.com>
 *
 */
class LDmPdfFilter extends CFilter
{
	
	/**
	 * @var string The $_GET variable key that contains the suffix value. Defaults to "suffix".
	 */
	public $suffixVarName = 'suffix';
	
	/**
	 * @var string The suffix to look for. Defaults to ".pdf"
	 */
	public $suffix = '.pdf';
	
	/**
	 * @var string The route to forward to if after a suffix match is made. Defaults to "pdf" meaning the action named Pdf of the current controller.
	 */
	public $forwardToRoute = 'pdf';

	/**
	 * Performs the pre-action filtering.
	 * @param CFilterChain $filterChain the filter chain that the filter is on.
	 * @return boolean whether the filtering process should continue and the action
	 * should be executed.
	 */
	protected function preFilter($filterChain)
	{
		if(isset($_GET[$this->suffixVarName]) && strcasecmp($_GET[$this->suffixVarName], $this->suffix) === 0)
		{
			$filterChain->controller->forward($this->forwardToRoute, true);
		}
		return parent::preFilter($filterChain);
	}

}