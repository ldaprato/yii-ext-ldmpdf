<?php
/**
 * LDmPdf class file.
 *
 * @author Louis A. DaPrato <l.daprato@gmail.com>
 * @link https://lou-d.com
 * @copyright 2014 Louis A. DaPrato
 * @license The MIT License (MIT)
 * @since 1.0
 */

/**
 * An application component to simplify configuring and instantiating mPDF instances.
 * 
 * @author Louis A. DaPrato <l.daprato@gmail.com>
 * @since 1.0
 *
 */
class LDmPdf extends CApplicationComponent
{
	
	/**
	 * @var string The category that will be used for message translations
	 */
	public $tCategory = 'LDmPdf';
	
	/**
	 * @var integer Mode to set when creating new directories. Defaults to 0700 (owner rwx, group none and others none).
	 */
	public $newDirMode = 0700;
	
	/**
	 * @var string Path alias to the mPDF class.  Defaults to "application.vendors.mPDF.mpdf".
	 */
	public $classPathAlias = 'application.vendors.mPDF.mpdf';
	
	/**
	 * @var string runtime path used for mPDF's temp files. Defaults to "application.runtime.mPDF".
	 */
	public $runtimePathAlias = 'application.runtime.mPDF';
	
	/**
	 * @var array The constants to define before importing the mPDF class. Defaults to null.
	 */
	public $constants;
	
	/**
	 * @var array A list of path aliases which will be dealiased, initialized and defined as their respective constant when this component is initialized.
	 */
	public $pathConstants = array();
	
	/**
	 * @var array The arguments that will be passed to the mPDF class constructor.
	 */
	private $_constructorArgs = array();
	
	/**
	 * @var ReflectionClass The reflector for the mPDF class.
	 */
	private $_reflection;
	
	/**
	 * (non-PHPdoc)
	 * @see CApplicationComponent::init()
	 */
	public function init()
	{
		$this->initConstants();
		
		$this->initPathConstants();
		
		Yii::import($this->classPathAlias, true);
		
		$this->initConstructorArgs();
	}
	
	/**
	 * Initializes constants
	 */
	protected function initConstants()
	{
		self::defineConstants($this->constants);
	}
	
	/**
	 * Initializes path constants
	 */
	protected function initPathConstants()
	{
		$this->pathConstants = array_merge(
			array(
				'_MPDF_TEMP_PATH' => $this->runtimePathAlias.'.tmp',
				'_MPDF_TTFONTDATAPATH' => $this->runtimePathAlias.'.ttfontdata'
			),
			(array)$this->pathConstants
		);
		self::defineConstants(array_map(array($this, 'ensureDirPathAlias'), $this->pathConstants));
	}
	
	/**
	 * Helper function for finding and initializing a directory path from a path alias.
	 * 
	 * @param string $pathAlias A Yii style path alias
	 * @throws CException Thrown if: 
	 * 	The pathAlias is invalid. 
	 * 	The path does not exist and cannot be created.
	 * 	The path exists, but is not a directory.
	 *  The path exists and is a directory, but the path is not writable.
	 * @return string the path of the alias including trailing directory separator
	 */
	protected function ensureDirPathAlias($pathAlias)
	{
		$dir = Yii::getPathOfAlias($pathAlias);
		if($dir === false)
		{
			throw new CException(Yii::t($this->tCategory, "Invalid path alias."));
		}
		else if(is_dir($dir) === false)
		{
			if(file_exists($dir))
			{
				throw new CException(Yii::t($this->tCategory, "Invalid path '{dir}'. The path exists, but it is not a directory.", array('{dir}' => $dir)));
			}
			else if(mkdir($dir, $this->newDirMode, true) === false)
			{
				throw new CException(Yii::t($this->tCategory, "The path '{dir}' does not exist and could not be created. Make sure the current process has both read and write permission for the path.", array('{dir}' => $dir)));
			}
		}
		else if(is_writable($dir) === false)
		{
			throw new CException(Yii::t($this->tCategory, "The path '{dir}' exists and is a directory, but is not writable. Make sure the current process has write permission for the path.", array('{dir}' => $dir)));
		}
		return $dir.DIRECTORY_SEPARATOR;
	}
	
	/**
	 * Initializes constructor arguments
	 * This method will extract the arguments and their default values from the mPDF constructor via reflection.
	 * Any arguments set via the constructorArgs property of this class will override the defaults found via reflection
	 */
	protected function initConstructorArgs()
	{
		$args = (array)$this->_constructorArgs;
		$this->_constructorArgs = array();
		$parts = explode('.', $this->classPathAlias);
		$this->_reflection = new ReflectionClass(end($parts));
		foreach($this->_reflection->getConstructor()->getParameters() as $index => $param)
		{
			if(array_key_exists($param->name, $args))
			{
				$this->_constructorArgs[$param->name] = $args[$param->name];
			}
			else if(array_key_exists($index, $args))
			{
				$this->_constructorArgs[$param->name] = $args[$index];
			}
			else
			{
				$this->_constructorArgs[$param->name] = $param->getDefaultValue();
			}
		}
	}
	
	/**
	 * Sets the constructor arguments
	 * 
	 * @param array $args The constructor arguments
	 */
	public function setConstructorArgs($args)
	{
		self::mergeArgs($this->_constructorArgs, $args);
	}
	
	/**
	 * Gets the constructor arguments
	 * 
	 * @return array The constructor arguments
	 */
	public function getConstructorArgs()
	{
		return $this->_constructorArgs;
	}

	/**
	 * Creates a new mPDF instance based on this components current configuration
	 * @param $args,... The arguments which will be passed to the mPDF class constructor. 
	 * 	The arguments specified here will override any normally generated by this component's configuration.
	 * @return mpdf
	 */
	public function mpdf()
	{
		$args = $this->getConstructorArgs();
		self::mergeArgs($args, func_get_args());
 		return $this->_reflection->newInstanceArgs($args);
	}
	
	/**
	 * Defines a list of constants if the constant is not already defined
	 * 
	 * @param array $constants A list of constants to be defined where the key is the name of the constant and the value is the constants value.
	 */
	public static function defineConstants($constants)
	{
		foreach((array)$constants as $name => $value)
		{
			if(defined($name) === false)
			{
				define($name, $value);
			}
		}
	}
	
	/**
	 * Merges method or function arguments. 
	 * This method is the same as array_merge except if a key is not a string in "moreArgs" then the numeric position of the element is spliced into the first array "args"
	 * 
	 * @param array $args The array to merge arguments into
	 * @param array $moreArgs The arguments being merged into the arguments array
	 */
	public static function mergeArgs(&$args, $moreArgs)
	{
		$keys = array_keys($args);
		foreach((array)$moreArgs as $name => $value)
		{
			if(is_int($name) && $name < count($keys))
			{
				array_splice($args, $name, 1, array($keys[$name] => $value));
			}
			else
			{
				$args[$name] = $value;
			}
		}
	}

}
