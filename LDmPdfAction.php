<?php

/**
 * An action that will render a view as a PDF.
 * 
 * @author Louis A. DaPrato <l.daprato@gmail.com>
 *
 */
class LDmPdfAction extends CAction
{
	
	/**
	 * @var string the category that will be used for all message translations
	 */
	public $tCategory = 'LDmPdfAction';
	
	/**
	 * @var string The name of the mPdf application component. Defaults to 'ePdf'.
	 */
	public $mPdfComponent = 'ePdf';
	
	/**
	 * @var array Arguments to pass to constructor at runtime.
	 */
	public $constructorArgs = array();
	
	/**
	 * @var array list of parameters to pass to the view when it is rendered
	 */
	public $viewParams = array();
	
	/**
	 * @var string The name of the view to render as a PDF. Defaults to null meaning strip the suffix off the ID of this action.
	 */
	public $viewName;
	
	/**
	 * @var string the name of the variable to pass to the view being rendered which will 
	 * 	contain the mPdf instance being used to convert the rendered view content to a PDF.
	 * 	You may check for the presence of this variable and use it to further customize how your view will be rendered as a PDF.
	 */
	public $mPdfVarName = 'mpdf';
	
	/**
	 * @var callable A callback which will be passed this action and the mPDF instance just prior to WriteHTML.
	 */
	public $callback;
	
	/**
	 * @var string the suffix of this action. Defaults to ".pdf"
	 */
	public $suffix = '.pdf';
	
	/**
	 * @var string The file name to output the PDF as. Defaults to 'document.pdf'
	 */
	public $filename = 'document.pdf';
	
	/**
	 * @var string The destination option for outputting the PDF. Defaults to 'I'.
	 */
	public $destination = 'I';

	/**
	 * Runs this action
	 * @throws CException if the component named by {@see LDmPdfAction::mPdfComponent} could not be found or is disabled.
	 */
	public function run()
	{
		$mpdf = Yii::app()->getComponent($this->mPdfComponent);
		if($mpdf === null)
		{
			throw new CException(Yii::t($this->tCategory, 'The mPDF application component named "{name}" could not be found. Make sure that the component is configured properly and enabled.', array('{name}' => $this->mPdfComponent)));
		}
		$reflector = new ReflectionMethod($mpdf, 'mpdf');
		$mpdf = $reflector->invokeArgs($mpdf, $this->constructorArgs);
		if($this->callback !== null)
		{
			call_user_func($this->callback, $this, $mpdf);
		}
		$this->viewParams['mpdf'] = $mpdf;
		$mpdf->WriteHTML($this->getController()->render($this->viewName === null ? preg_replace('/'.preg_quote($this->suffix).'$/i', '', $this->getId()) : $this->viewName, $this->viewParams, true));
		$mpdf->Output($this->filename, $this->destination);
	}

}

?>